var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mysql = require('./mysql/');
var dirname = __dirname.replace("server","");

app.use(express.static(dirname + '/'));

app.get('/', function(req, res){
  res.sendFile(dirname + '/index.html');
});


mysql.dataMysql.pool.getConnection(function (err, connection) {
    if (!err) {
        return true;
    } else {
        app.get('/error', function (req, res) {
            res.sendFile(__dirname + '/error.html');
        });

    }
});

io.on('connection', function(socket){
    /* SERVER LOGIN LISTENING */
  	  socket.on('login user', function (inforLogin) {
    		mysql.dataMysql.pool.query('SELECT * FROM users WHERE `email` = ?', [inforLogin.email], function (err, results, fileds) {
    			if (!err) {
    				if (mysql.passwordHash.checkPassword(inforLogin.password, results[0].password)) {
    					delete results[0]["password"];
    					rsQuery = {
    						'status': 'ok',
    						'data': results[0]
    					}

    				}else{
    					rsQuery = false;
    				}
    			} else rsQuery = false;
    			socket.emit('login user', rsQuery);
    		});
    	})
        
      /* SERVER SENDMESSAGE LISTENING */ 
      socket.on('chat message', function(msg){
        io.emit('chat message', msg);
      });
});


http.listen(3000, function(){
  console.log('listening on *:3000');
});
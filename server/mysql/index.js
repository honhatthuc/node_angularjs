/*
**
@ Defined mysql private
**/
module.exports.dataMysql = module.exports.dataMysql || {};
module.exports.PasswordHash = require('phpass').PasswordHash;
module.exports.passwordHash = new module.exports.PasswordHash();

/*
**
@ Value Message Query Mysql
**/

module.exports.dataMysql.isDataConnect = 'Database is connected ... ';

module.exports.dataMysql.notDataConnect = 'Error connecting database ...';

module.exports.dataMysql.errorQuery = 'Error while performing Query.';

module.exports.dataMysql.successQuery = 'Query successfully !';



/*
**
@ Require Node Mysql
**/

module.exports.dataMysql.nodeMySQL = require('mysql');

/*
**
@ Connect node database Mysql
**/

module.exports.dataMysql.pool = module.exports.dataMysql.nodeMySQL.createPool({
	connectionLimit: 1000, //important
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'socket',
	debug: false
});


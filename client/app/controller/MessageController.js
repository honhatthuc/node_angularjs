myapp.controller('MessageController', function ($scope, $location, $timeout, $rootScope , socket) {
    
    $scope.listMessage = [];
    $scope.message = {};
    $scope.notification = [];
    $scope.myself = JSON.parse(localStorage.getItem('myself'));
    
    $scope.sendMessage = function () {
        socket.emit('chat message', {
            'idUser': $scope.myself.idUser,
            'nameUser': $scope.myself.userName,
            'avatar': $scope.myself.avatar,
            'message': $scope.message.content
        });
        $scope.returnEmptyMs();
    }
  
    $scope.returnEmptyMs = function(){
         $scope.message.content = '';
         return false;
    }
    socket.on('chat message', function (msg) {
        $timeout(function () {
            $scope.listMessage.push(msg)
        },0);
        console.log($scope.listMessage);
    });

});

myapp.controller('LoginController', function($scope,$location,$timeout,socket){
    $scope.islogin = false;
    $scope.dataUserCurrent = $scope.dataUserCurrent || {};
    if (typeof localStorage === "undefined" || localStorage === null) {
        var LocalStorage = require('node-localstorage').LocalStorage;
        localStorage = new LocalStorage('./scratch');
    }
    $scope.Login = function () {

        if (!$scope.islogin) {
            if (typeof $scope.email == 'undefined')
                return false;
            else {
                if (typeof $scope.password == 'undefined')
                    return false;
                else {
                    socket.emit('login user', {
                        'email': $scope.email,
                        'password': $scope.password
                    });
                }
            }
        } else {
            return myMessageCilent.connectedAgo;
        }
    }
  
    socket.on('login user', function (dataCallBack) {
        if (dataCallBack) {
            if (dataCallBack.status == 'ok') {
                $scope.islogin = true;
                $scope.dataUserCurrent = dataCallBack.data;
                localStorage.setItem('myself', JSON.stringify(dataCallBack.data));
                $timeout(function () {
                  $location.path('/message');
                })

            }
        } else {
            return false;
        }
    })
})


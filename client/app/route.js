myapp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: 'client/app/views/login.html',
        controller: 'LoginController'
      }).
      when('/message', {
        templateUrl: 'client/app/views/message.html',
        controller: 'MessageController'
      }).
      otherwise({
        redirectTo: '/errors'
      });
  }]);